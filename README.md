# Frontend Mentor - Multi-step form solution

This is a solution to the [Multi-step form challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/multistep-form-YVAnSdqQBJ). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Frontend Mentor - Multi-step form solution](#frontend-mentor---multi-step-form-solution)
  - [Table of contents](#table-of-contents)
  - [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
  - [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Useful resources](#useful-resources)
  - [Author](#author)

## Overview

### The challenge

Users should be able to:

- Complete each step of the sequence
- Go back to a previous step to update their selections
- See a summary of their selections on the final step and confirm their order
- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page
- Receive form validation messages if:
  - A field has been missed
  - The email address is not formatted correctly
  - A step is submitted, but no selection has been made

### Screenshot

![preview of the form in desktop which consist of 3 input field with a step info on the left and next button on bottom-right](./public/preview.png)

### Links

- Solution URL: [Multi Step Form Repository on GitLab](https://gitlab.com/aenjojo/multi-step-form)
- Live Site URL: [Multi Step Form Live Site on Netlify](https://aenjojo-multi-step-form.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- Flexbox
- Typescript
- [React](https://react.dev/) - JS library
- [Redux-Toolkit](https://redux-toolkit.js.org/) - Global state management
- [Sass](https://sass-lang.com/) - For styles
- [Vite](https://vitejs.dev) - For bundler

### What I learned

By completing this project, I learned how to slice a design into code by just looking up on the given images of the screens design.

I also learned how to manage a state globally with redux, because I modularize every page to make the development easier with reusability and encapsulation. Here, I don't use the plain redux, but redux-toolkit to make sure I create the reducer correctly.

When designing the form, I intended to use Sass so I can learn it while following the customized design requirement.

### Useful resources

- [How to Use Redux and Redux Toolkit – Tutorial for Beginners](https://www.freecodecamp.org/news/redux-and-redux-toolkit-for-beginners/) - This article helped me to set up the reducer with redux-toolkit (I follow the official documentation too).
- [CSS Tools: Reset CSS](https://meyerweb.com/eric/tools/css/reset/) - This helped me to reset the default css styling. Other people which need a css style reset should use it too.
- [How To Create a Custom Checkbox/Radio](https://www.w3schools.com/howto/howto_css_custom_checkbox.asp) - This helped me how to properly redesign the checkbox and radio input.

## Author

- Website - [Aen Jojo Personal Website](https://aenjojo.dev)
- Frontend Mentor - [@aenjojo](https://www.frontendmentor.io/profile/aenjojo)
- Twitter - [@aenjojo_](https://www.twitter.com/aenjojo_)
