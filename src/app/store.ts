import { configureStore, combineReducers } from '@reduxjs/toolkit';
import planReducer from '../features/plan/planSlice';
import addonReducer from '../features/addon/addonSlice';

const rootReducer = combineReducers({
	plan: planReducer,
	addon: addonReducer,
});

export const store = configureStore({
	reducer: rootReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch;