import { Plan } from '../features/plan/Plan';
import { AddOn } from '../features/addon/AddOn';
import { useState } from 'react';
import { StepLayout } from '../components/layout/StepLayout';
import { Form, FormData, FormKey } from '../components/ui/Form';
import { Confirmation } from '../components/ui/Confirmation';
import { Sidebar } from '../components/layout/SidebarLayout';
import { ThankYou } from '../components/ui/ThankYou';

const initialFormData: FormData = {
	name: {
		value: '',
		invalid: false,
		pattern: /^[\w ]{2,}$/,
	},
	email: {
		value: '',
		invalid: false,
		pattern: /^[\w.-]+@[\w-]+\.[\w]+$/,
	},
	phone: {
		value: '',
		invalid: false,
		pattern: /^\+?\d{7,}$/,
	},
};

export function App() {
	const [page, setPage] = useState<number>(1);
	const [formData, setFormData] = useState<FormData>(initialFormData);

	const handleChange = (name: FormKey) => {
		return (event: React.ChangeEvent<HTMLInputElement>) => {
			setFormData({
				...formData,
				[name]: {
					...formData[name],
					value: event.target.value,
					invalid: !event.target.value.match(formData[name].pattern) ? true : false,
				},
			});
		};
	};

	const prevPage = () => {
		setPage(page => page - 1);
	};
	
	const nextPage = () => {
		if (page === 1
			&& (!formData.name.value.match(formData.name.pattern)
				|| !formData.email.value.match(formData.email.pattern)
				|| !formData.phone.value.match(formData.phone.pattern)
			)
		) {
			setFormData({
				name: {
					...formData.name,
					invalid: !formData.name.value.match(formData.name.pattern) ? true : false,
				},
				email: {
					...formData.email,
					invalid: !formData.email.value.match(formData.email.pattern) ? true : false,
				},
				phone: {
					...formData.phone,
					invalid: !formData.phone.value.match(formData.phone.pattern) ? true : false,
				},
			});
		}
		else {
			setPage(page => page + 1);
		}
	};

	const toPlanPage = () => {
		setPage(2);
	};

	const renderPage = () => {
		switch (page) {
			case 1: return (
				<StepLayout
					nextPageFunc={nextPage}
				>
					<Form
						formState={formData}
						changeHandler={handleChange}
					/>
				</StepLayout>
			);
			case 2: return (
				<StepLayout
					prevPageFunc={prevPage}
					nextPageFunc={nextPage}
				>
					<Plan />
				</StepLayout>
			);
			case 3: return (
				<StepLayout
					prevPageFunc={prevPage}
					nextPageFunc={nextPage}
				>
					<AddOn />
				</StepLayout>
			);
			case 4: return (
				<StepLayout
					prevPageFunc={prevPage}
					confirmFunc={nextPage}
				>
					<Confirmation changePageFunc={toPlanPage} />
				</StepLayout>
			);
			case 5: return (
				<StepLayout>
					<ThankYou />
				</StepLayout>
			);
		}
	};

	return (
		<div className='main-container'>
			<Sidebar currentPage={page} />
			<div className='main-form__container'>
				{renderPage()}
			</div>
		</div>
	);
}