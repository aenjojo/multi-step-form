type ButtonProps = {
	label: string;
	variant: 'primary' | 'secondary' | 'tertiary';
} & React.ButtonHTMLAttributes<HTMLButtonElement>

export function Button({ label, variant, ...props }: ButtonProps) {
	return (
		<button
			className={`button button--${variant}`}
			{...props}
		>
			{label}
		</button>
	);
}