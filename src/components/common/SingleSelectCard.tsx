type SingleSelectCardProps = {
	label: {
		title: string;
		description: string;
		note?: string;
	};
	name: string;
	checked?: boolean;
	icon?: string;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export function SingleSelectCard({ label, name, checked, icon, onChange }: SingleSelectCardProps) {
	return (
		<label className='single-select__container'>
			<input
				type='radio'
				name={name}
				className='single-select__input'
				checked={checked}
				onChange={onChange}
			/>
			<img
				src={icon}
				alt=''
				className='single-select__icon'
			/>
			<span className='single-select__label'>
				<span className='single-select__label-title'>
					{label.title}
				</span>
				<span className='single-select__label-description'>
					{label.description}
				</span>
				{label.note ? (
					<span className='single-select__label-note'>
						{label.note}
					</span>
				) : false}
			</span>
		</label>
	);
}