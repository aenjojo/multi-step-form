import iconCheckmark from '../../assets/images/icon-checkmark.svg';

type MultiSelectCardProps = {
	label: {
		title: string;
		description: string;
	};
	price: string;
	name: string;
	checked?: boolean;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export function MultiSelectCard({ label, price, name, checked, onChange }: MultiSelectCardProps) {
	return (
		<label className='multi-select__container'>
			<input
				type='checkbox'
				name={name}
				className='multi-select__input'
				checked={checked}
				onChange={onChange}
			/>
			<span className='multi-select__icon'>
				<img
					src={iconCheckmark}
					alt=''
					className='multi-select__icon-image'
				/>
			</span>
			<span className='multi-select__label'>
				<span className='multi-select__label-title'>
					{label.title}
				</span>
				<span className='multi-select__label-description'>
					{label.description}
				</span>
			</span>
			<span className='multi-select__price'>
				{price}
			</span>
		</label>
	);
}