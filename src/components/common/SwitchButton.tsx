type SwitchButtonProps = {
	labelLeft: string;
	labelRight: string;
	checked?: boolean;
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export function SwitchButton({ labelLeft, labelRight, checked, onChange }: SwitchButtonProps) {
	return (
		<label className='switch__container'>
			<input
				type='checkbox'
				name=''
				className='switch__input'
				checked={checked}
				onChange={onChange}
			/>
			<span className='switch__label'>
				<span className='switch__label-left'>
					{labelLeft}
				</span>
				<span className='switch__button-track'>
					<span className='switch__button-thumb'></span>
				</span>
				<span className='switch__label-right'>
					{labelRight}
				</span>
			</span>
		</label>
	);
}