type InputFieldProps = {
	label: string;
	name: string;
	invalid?: boolean;
	invalidMessage?: string,
} & React.InputHTMLAttributes<HTMLInputElement>

export function InputField({ label, name, invalid, invalidMessage, ...props }: InputFieldProps) {
	return (
		<div className='input__container'>
			<label
				className='input__label'
				htmlFor={name}
			>
				<span>
					{label}
				</span>
				<span className={`input__message ${invalid ? 'input__message--invalid' : ''}`}>
					{invalidMessage}
				</span>
			</label>
			<input
				className={`input__field ${invalid ? 'input__field--invalid' : ''}`}
				id={name}
				{...props}
			/>
		</div>
	);
}