type PageData = {
	step: number;
	title: string;
}

type SidebarProps = {
	currentPage: number;
}

const pageData: PageData[] = [{
	step: 1,
	title: 'Your Info',
}, {
	step: 2,
	title: 'Select Plan',
}, {
	step: 3,
	title: 'Add-ons',
}, {
	step: 4,
	title: 'Summary',
}];

export function Sidebar({ currentPage }: SidebarProps) {
	const pageList = pageData.map(data => {
		const activePage = (currentPage === 5 && data.step === 4) || currentPage === data.step ? 'sidebar-menu__item-marker--active' : '';

		return (
			<div
				key={data.step}
				className='sidebar-menu__item'
			>
				<div
					className={`sidebar-menu__item-marker ${activePage}`}
				>
					<span>
						{data.step}
					</span>
				</div>
				<div className='sidebar-menu__item-description'>
					<p className='sidebar-menu__item-name'>
						Step {data.step}
					</p>
					<p className='sidebar-menu__item-title'>
						{data.title}
					</p>
				</div>
			</div>
		);
	});

	return (
		<div className='sidebar-menu__container'>
			{pageList}
		</div>
	);
}