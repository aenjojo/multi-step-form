type OptionLayoutProps = {
	children: React.ReactNode | React.ReactNode[];
	optionDirection: 'row' | 'column';
}

export function OptionLayout({ children, optionDirection }: OptionLayoutProps) {
	return (
		<div className={`option-layout option-layout--desktop-${optionDirection}`}>
			{children}
		</div>
	);
}