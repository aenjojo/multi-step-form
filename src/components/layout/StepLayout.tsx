import { Button } from '../common/Button';

type StepLayoutProps = {
	children: React.ReactNode | React.ReactNode[];
	prevPageFunc?(): void;
	nextPageFunc?(): void;
	confirmFunc?(): void;
}

export function StepLayout({ children, prevPageFunc, nextPageFunc, confirmFunc }: StepLayoutProps) {
	return (
		<div className='form-layout__container'>
			<div className='form-layout__step-container'>
				<div className='form-layout__step-form'>
					{children}
				</div>
			</div>
			<div className='form-layout__button-container'>
				{confirmFunc ? (
					<Button
						label='Confirm'
						variant='primary'
						onClick={confirmFunc}
					/>
				) : false}
				{nextPageFunc ? (
					<Button
						label='Next Step'
						variant='secondary'
						onClick={nextPageFunc}
					/>
				) : false}
				{prevPageFunc ? (
					<Button
						label='Go Back'
						variant='tertiary'
						onClick={prevPageFunc}
					/>
				) : false}
			</div>
		</div>
	);
}