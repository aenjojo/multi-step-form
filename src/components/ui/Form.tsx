import { InputField } from '../common/InputField';
import { OptionLayout } from '../layout/OptionLayout';

export type FormKey = 'name' | 'email' | 'phone'

export type FormData = {
	[key in FormKey]: {
		value: string;
		invalid: boolean;
		pattern: RegExp;
	};
}

type FormProps = {
	formState: FormData;
	changeHandler: (name: FormKey) => (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export function Form({ formState, changeHandler }: FormProps) {
	return (
		<>
			<h1>Personal info</h1>
			<p>Please provide your name, email address, and phone number.</p>
			<OptionLayout optionDirection='column'>
				<InputField
					label='Name'
					name='name'
					type='text'
					placeholder='e.g. Stephen King'
					value={formState['name'].value}
					invalid={formState['name'].invalid}
					invalidMessage='Must be 2 characters or more'
					onChange={changeHandler('name')}
				/>
				<InputField
					label='Email Address'
					name='email'
					type='email'
					placeholder='e.g. stephenking@lorem.com'
					value={formState['email'].value}
					invalid={formState['email'].invalid}
					invalidMessage='Invalid email'
					onChange={changeHandler('email')}
				/>
				<InputField
					label='Phone Number'
					name='phone'
					type='tel'
					placeholder='e.g. +1 234 567 890'
					value={formState['phone'].value}
					invalid={formState['phone'].invalid}
					invalidMessage='Must be 7 digit numbers and more'
					onChange={changeHandler('phone')}
				/>
			</OptionLayout>
		</>
	);
}