import { useSelector } from 'react-redux';
import type { RootState } from '../../app/store';

type ConfirmationProps = {
	changePageFunc(): void;
}

export function Confirmation({ changePageFunc }: ConfirmationProps) {
	const { plan, addon } = useSelector((state: RootState) => state);

	const subscriptionCode = plan.subscription === 'yearly' ? 'yr' : 'mo';

	const sortedAddon = [...addon].sort((prev, next) => prev.id - next.id);
	const addonsPrice = sortedAddon.reduce((total, current) => total + current.price, 0);
	const totalPrice = plan.price + addonsPrice;

	return (
		<>
			<h1>Finishing up</h1>
			<p>Double-check everything looks OK before confirming.</p>
			<div className='detail-container'>
				<div className='detail-plan'>
					<div className='detail-plan__description'>
						<span className='detail-plan__name'>
							{plan.name} ({plan.subscription})
						</span>
						<span className='detail-plan__button' onClick={changePageFunc}>
							Change
						</span>
					</div>
					<div className='detail-plan__price'>
						${plan.price}/{subscriptionCode}
					</div>
				</div>
				<hr className='detail-line' />
				<div className='detail-addon'>
					{sortedAddon.map(({ id, name, price }) => (
						<p key={id} className='detail-addon__item'>
							<span className='detail-addon__item-name'>
								{name}
							</span>
							<span className='detail-addon__item-price'>
								+${price}/{subscriptionCode}
							</span>
						</p>
					))}
				</div>
			</div>
			<div className='total-container'>
				<span className='total-description'>
					Total (per {plan.subscription.substring(0, plan.subscription.length - 2)})
				</span>
				<span className='total-price'>
					${totalPrice}/{subscriptionCode}
				</span>
			</div>
		</>
	);
}