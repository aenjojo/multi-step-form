import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

export type AddOnId = {
	id: number;
};

export type AddOnState = AddOnId & {
	name: string;
	price: number;
}

const initialState: AddOnState[] = [];

const addonSlice = createSlice({
	name: 'addon',
	initialState,
	reducers: {
		addAddon: (state, action: PayloadAction<AddOnState>) => {
			state.splice(action.payload.id, 0, {
				id: action.payload.id,
				name: action.payload.name,
				price: action.payload.price,
			});
		},
		removeAddon: (state, action: PayloadAction<AddOnId>) => {
			return state.filter(addon => addon.id !== action.payload.id);
		},
		updatePrice: (state, action: PayloadAction<{ updateType: 'yearly' | 'monthly' }>) => {
			const yearlyPrice = 10;

			state.forEach(addon => (
				addon.price = action.payload.updateType === 'yearly'
					? addon.price * yearlyPrice
					: addon.price / yearlyPrice
			));
		},
	},
});

export const { addAddon, removeAddon, updatePrice } = addonSlice.actions;
export default addonSlice.reducer;