export const addonData = [
	{
		id: 0,
		name: 'Online Service',
		desc: 'Access to multiplayer games',
		subs: {
			monthly: 1,
			yearly: 10,
		},
	}, {
		id: 1,
		name: 'Larger Storage',
		desc: 'Extra 1TB of cloud save',
		subs: {
			monthly: 2,
			yearly: 20,
		},
	}, {
		id: 2,
		name: 'Customizable Profile',
		desc: 'Custom theme on your profile',
		subs: {
			monthly: 2,
			yearly: 20,
		},
	},
];