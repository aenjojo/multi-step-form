import { useSelector, useDispatch } from 'react-redux';
import { addAddon, removeAddon, AddOnId, AddOnState } from './addonSlice';
import type { RootState } from '../../app/store';
import { MultiSelectCard } from '../../components/common/MultiSelectCard';
import { OptionLayout } from '../../components/layout/OptionLayout';
import { addonData } from './addonData';

export function AddOn() {
	const subscription = useSelector((state: RootState) => state.plan.subscription);
	const addons = useSelector((state: RootState) => state.addon);
	const dispatch = useDispatch();

	const subscriptionCode = subscription === 'yearly' ? 'yr' : 'mo';

	const onChange = (event: React.ChangeEvent<HTMLInputElement>, data: AddOnId & AddOnState) => {
		if (event.target.checked) {
			dispatch(addAddon({
				id: data.id,
				name: data.name,
				price: data.price,
			}));
		}
		else {
			dispatch(removeAddon({
				id: data.id,
			}));
		}
	};

	const checkActivated = (id: number) => {
		const found = addons.find(addon => addon.id === id);

		if (found) {
			return true;
		}
		else {
			return false;
		}
	};

	const addonList = addonData.map(addon => {
		const id = addon.id;
		const name = addon.name;
		const nameId = name.toLowerCase();
		const price = addon.subs[subscription];
		const priceDesc = `+$${price}/${subscriptionCode}`;

		return (
			<MultiSelectCard
				key={`${name}-${price}`}
				label={{
					title: name,
					description: addon.desc,
				}}
				price={priceDesc}
				name='addon'
				checked={checkActivated(id)}
				onChange={(evt) => onChange(evt, {
					id: id,
					name: nameId,
					price: price,
				})}
			/>
		);
	});

	return (
		<>
			<h1>Pick add-ons</h1>
			<p>Add-ons help enhance your gaming experience.</p>
			<OptionLayout optionDirection='column'>
				{addonList}
			</OptionLayout>
		</>
	);
}