import { useSelector, useDispatch } from 'react-redux';
import { selectPlan, selectSubsription } from './planSlice';
import type { RootState } from '../../app/store';
import { SingleSelectCard } from '../../components/common/SingleSelectCard';
import { SwitchButton } from '../../components/common/SwitchButton';
import { OptionLayout } from '../../components/layout/OptionLayout';
import { updatePrice } from '../addon/addonSlice';
import { planData } from './planData';

export function Plan() {
	const plan = useSelector((state: RootState) => state.plan);
	const dispatch = useDispatch();
	
	const subscriptionCode = plan.subscription === 'yearly' ? 'yr' : 'mo';

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		const yearlyPrice = 10;

		if (event.target.checked) {
			dispatch(selectSubsription({
				price: plan.price * yearlyPrice,
				subscription: 'yearly',
			}));
			dispatch(updatePrice({ updateType: 'yearly' }));
		}
		else {
			dispatch(selectSubsription({
				price: plan.price / yearlyPrice,
				subscription: 'monthly',
			}));
			dispatch(updatePrice({ updateType: 'monthly' }));
		}
	};

	const planList = planData.map(data => {
		const name = data.name;
		const nameId = name.toLowerCase();
		const price = data.subs[plan.subscription];
		const description = `$${price}/${subscriptionCode}`;
		const note = plan.subscription === 'yearly'
			? '2 months free'
			: undefined;

		return (
			<SingleSelectCard
				key={`${name}-${price}`}
				label={{
					title: name,
					description: description,
					note: note,
				}}
				name='plan'
				checked={plan.name === nameId}
				icon={data.icon}
				onChange={() => dispatch(selectPlan({
					name: nameId,
					price: price,
				}))}
			/>
		);
	});

	return (
		<>
			<h1>Select your plan</h1>
			<p>You have the option of monthly or yearly billing.</p>
			<OptionLayout optionDirection='row'>
				{planList}
			</OptionLayout>
			<div>
				<SwitchButton
					labelLeft='Monthly'
					labelRight='Yearly'
					checked={plan.subscription === 'yearly'}
					onChange={handleChange}
				/>
			</div>
		</>
	);
}