import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

export type PlanState = {
	name: string;
	price: number;
}

export type SubscriptionState = {
	subscription: 'monthly' | 'yearly';
}

const initialState: PlanState & SubscriptionState = {
	name: 'arcade',
	price: 9,
	subscription: 'monthly',
};

const planSlice = createSlice({
	name: 'plan',
	initialState,
	reducers: {
		selectPlan: (state, action: PayloadAction<PlanState>) => {
			state.name = action.payload.name;
			state.price = action.payload.price;
		},
		selectSubsription: (state, action: PayloadAction<SubscriptionState & Pick<PlanState, 'price'>>) => {
			state.price = action.payload.price;
			state.subscription = action.payload.subscription;
		},
	},
});

export const { selectPlan, selectSubsription } = planSlice.actions;
export default planSlice.reducer;