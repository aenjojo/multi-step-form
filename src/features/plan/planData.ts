import iconArcade from '../../assets/images/icon-arcade.svg';
import iconAdvanced from '../../assets/images/icon-advanced.svg';
import iconPro from '../../assets/images/icon-pro.svg';

export const planData = [
	{
		name: 'Arcade',
		subs: {
			monthly: 9,
			yearly: 90,
		},
		icon: iconArcade,
	}, {
		name: 'Advanced',
		subs: {
			monthly: 12,
			yearly: 120,
		},
		icon: iconAdvanced,
	}, {
		name: 'Pro',
		subs: {
			monthly: 15,
			yearly: 150,
		},
		icon: iconPro,
	}
];